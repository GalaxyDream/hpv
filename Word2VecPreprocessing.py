import json
from pprint import pprint
import numpy as np
import os
import common

def ParseJsonText(fileJ):
	json_file = np.loadtxt(fileJ, delimiter='\n', dtype=str)
	text=''
	for i in range(len(json_file)):
		if(len(json_file[i])==0 or json_file[i][len(json_file[i])-1] != '}'):
			continue
		json_parsed = json.loads(json_file[i])
		text+=' '+json_parsed['text']
	return text

def ParseHpv(TopDir):
	dir1=os.listdir(TopDir)
	dir1.sort()
	TextTotal=''
	for i in range(len(dir1)):
		print('processing...'+str(i))
		dir2=os.listdir(TopDir+dir1[i])
		TextTotal+=' '+ParseJsonText(TopDir+dir1[i]+'/'+dir2[0])
	return TextTotal

txt = ParseHpv('./hpvtest/')
txt = common.sanitize_text(txt)

RawText = open('rawtext.txt','w+')
RawText.write(txt)
RawText.flush()